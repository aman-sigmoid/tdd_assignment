#  Class name is not meaningful. It has to be like FactorialFinder
class FactorialFinder:

    def fac(self,num):
        if num>=0:
            fact = 1
            for i in range(1, num + 1):
                fact = fact * i
            return fact

        else:
            raise Exception("Negative value")


    def readFromFile(self,filename):
        infile=open(filename,"r")
        line=infile.readline()
        return line


obj=FactorialFinder()
print(obj.fac(int(obj.readFromFile("_input"))))